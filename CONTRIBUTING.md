This is a full stack application consisting of:
* `Client-side Implementation` : Flutter Android App
* `Back-end Authentication` : Firebase
* `Database` : Firestore nosql DB

## Making changes

Before making a change, create a branch off of `master` with one of the following prefixes to describe it's purpose:
* `feat/`: For adding a new feature.
* `fix/`: For fixing a bug.
* `ref/`: For any architectural changes, moving or renaming files/directories.
* `docs/`: For any purely documentation related changes

## Git Commits

The following [emojis](https://gitmoji.carloscuesta.me/) are to be prefixed with a short and concise commit title:
* :art: `:art:` Improving structure / format of the code.
* :zap: `:zap:` Improving performance.
* :fire: `:fire:` Removing code or files.
* :bug: `:bug:` Fixing a bug.
* :ambulance: `:ambulance:` Critical hotfix.
* :sparkles: `:sparkles:` Introducing new features.
* :pencil: `:pencil:` Writing docs.
* :lipstick: `:lipstick:` Updating the UI and style files.
* :white_check_mark: `:white_check_mark:` Updating tests.
* :lock: `:lock:` Fixing security issues.
* :construction: `:construction:` Work in progress.
* :recycle: `:recycle:` Refactoring code.
* :chart_with_upwards_trend: `:chart_with_upwards_trend:` Adding analytics or tracking code.
* :heavy_plus_sign: `:heavy_plus_sign:` Adding a dependency.
* :heavy_minus_sign: `:heavy_minus_sign:` Removing a dependency.
* :wrench: `:wrench:` Changing configuration files.
* :globe_with_meridians: `:globe_with_meridians:` Internationalization and localization.
* :pencil2: `:pencil2:` Fixing typos.
* :twisted_rightwards_arrows: `:twisted_rightwards_arrows:` Merging branches.
* :truck: `:truck:` Moving or renaming files.
* :bento: `:bento:` Adding or updating assets.
* :card_file_box: `:card_file_box:` Performing database related changes.
* :building_construction: `:building_construction:` Making architectural changes.
* :see_no_evil: `:see_no_evil:` Adding or updating .gitignore file.
* :goal_net: `:goal_net:` Catching errors.
* :dizzy: `:dizzy:` Adding or updating animations and transitions

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:think_ninjas_barcode/models/category.dart';
import 'package:think_ninjas_barcode/models/product.dart';
import 'package:think_ninjas_barcode/models/retailer.dart';
import 'package:think_ninjas_barcode/models/unit_of_measurement.dart';
import 'package:think_ninjas_barcode/models/userScan.dart';

class DatabaseService {
  final String userUID;
  DatabaseService({this.userUID});

  // collection references
  final CollectionReference userScansCollection =
      Firestore.instance.collection('userScans');
  final CollectionReference productCollection =
      Firestore.instance.collection('product');
  final CollectionReference categoryCollection =
      Firestore.instance.collection('foodCategory');
  final CollectionReference unitCollection =
      Firestore.instance.collection('unitOfMeasurement');
  final CollectionReference retailerCollection =
      Firestore.instance.collection('retailer');

  // user's scan list from snapshot
  List<UserScan> _userScansFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.documents.map((doc) {
      return UserScan(
          userUID: doc.data['userUID'] ?? '', scans: doc.data['scans'] ?? []);
    }).toList();
  }

  // get userScans stream
  Stream<List<UserScan>> get userScans {
    return userScansCollection.snapshots().map(_userScansFromSnapshot);
  }

  // product list from snapshot
  List<Product> _productsFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.documents.map((doc) {
      return Product(
          barcodeID: doc.data['barcodeID'] ?? '',
          categoryID: doc.data['categoryID'] ?? '',
          category: doc.data['category'] ?? '',
          name: doc.data['name'] ?? '',
          price: doc.data['price'].toDouble() ?? 0.0,
          quantity: doc.data['quantity'].toDouble() ?? 0.0,
          unitOfMeasurementID: doc.data['unitOfMeasurementID'] ?? '',
          unitOfMeasurement: doc.data['unitOfMeasurement'] ?? '',
          retailerID: doc.data['retailerID'] ?? '',
          retailer: doc.data['retailer'] ?? '',
          retailerBranch: doc.data['retailerBranch'] ?? '');
    }).toList();
  }

  // get products stream
  Stream<List<Product>> get products {
    return productCollection.snapshots().map(_productsFromSnapshot);
  }

  // categories list from snapshot
  List<Category> _categoriesFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.documents.map((doc) {
      return Category(
          name: doc.data['name'] ?? '',
          unitOfMeasurementID: doc.data['unitOfMeasurementID'] ?? '');
    }).toList();
  }

  // get categories stream
  Stream<List<Category>> get categories {
    return categoryCollection.snapshots().map(_categoriesFromSnapshot);
  }

  // units list from snapshot
  List<Unit> _unitsFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.documents.map((doc) {
      return Unit(code: doc.data['code'] ?? '', name: doc.data['name'] ?? '');
    }).toList();
  }

  // get units stream
  Stream<List<Unit>> get units {
    return unitCollection.snapshots().map(_unitsFromSnapshot);
  }

  // retailers list from snapshot
  List<Retailer> _retailersFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.documents.map((doc) {
      return Retailer(
          name: doc.data['name'] ?? '',
          branches: doc.data['retailerBranches'] ?? []);
    }).toList();
  }

  // get retailers stream
  Stream<List<Retailer>> get retailers {
    return retailerCollection.snapshots().map(_retailersFromSnapshot);
  }

  getUserScanList(String userUID) async {
    DocumentReference docRef =
        Firestore.instance.collection('userScans').document(userUID);
    DocumentSnapshot doc = await docRef.get();
    List scans = doc.data['scans'];
    return scans;
  }

  Future createUserScans(String userUID, var scanArr) async {
    return await userScansCollection.document(userUID).setData({
      'userUID': userUID,
      'scans': scanArr,
    });
  }

  Future addUserScans(String userUID, String barcodeID) async {
    List scans = await getUserScanList(userUID);
    if (scans.contains(barcodeID)) {
      await removeUserScans(userUID, barcodeID);
      return await addUserScans(userUID, barcodeID);
    } else {
      return await Firestore.instance
          .collection('userScans')
          .document(userUID)
          .updateData({
        'scans': FieldValue.arrayUnion([barcodeID])
      });
    }
  }

  Future removeUserScans(String userUID, String barcodeID) async {
    return await Firestore.instance
        .collection('userScans')
        .document(userUID)
        .updateData({
      'scans': FieldValue.arrayRemove([barcodeID])
    });
  }

  Future getProductWithBarcode(String barcodeID) async {
    return await Firestore.instance
        .collection('product')
        .where("barcodeID", isEqualTo: barcodeID)
        .limit(1)
        .getDocuments()
        .then(
      (value) {
        if (value.documents.length > 0) {
          return value.documents[0].data;
        } else {
          return null;
        }
      },
    );
  }

  Future getCategoryID(String category) async {
    return await Firestore.instance
        .collection('foodCategory')
        .where("name", isEqualTo: category)
        .limit(1)
        .getDocuments()
        .then(
      (value) {
        if (value.documents.length > 0) {
          return value.documents[0].documentID;
        } else {
          return null;
        }
      },
    );
  }

  Future getCategory(String categoryID) async {
    DocumentReference docRef =
        Firestore.instance.collection('foodCategory').document(categoryID);
    DocumentSnapshot doc = await docRef.get();
    String category = doc.data['name'];
    return category;
  }

  Future addCategory(String category) async {
    if (await getCategoryID(category) == null) {
      return await Firestore.instance
          .collection('foodCategory')
          .add({'name': category, 'unitOfMeasurementID': ''});
    } else {
      return null;
    }
  }

  Future doesCategoryHaveUnit(String category) async {
    String categoryID = await getCategoryID(category);
    DocumentReference docRef =
        Firestore.instance.collection('foodCategory').document(categoryID);
    DocumentSnapshot doc = await docRef.get();
    String unit = doc.data['unitOfMeasurementID'];
    if (unit != "") {
      return true;
    } else {
      return false;
    }
  }

  Future doesCategoryExist(String category) async {
    return await Firestore.instance
        .collection('foodCategory')
        .where("name", isEqualTo: category)
        .limit(1)
        .getDocuments()
        .then(
      (value) {
        if (value.documents.length > 0) {
          return true;
        } else {
          return false;
        }
      },
    );
  }

  Future updateCategoryWithUnit(
      String categoryInputValue, String unitCodeInputValue) async {
    String categoryID = await getCategoryID(categoryInputValue);
    String unitOfMeasurementID =
        await getUnitOfMeasurementID(unitCodeInputValue);
    return await Firestore.instance
        .collection('foodCategory')
        .document(categoryID)
        .updateData({'unitOfMeasurementID': unitOfMeasurementID});
  }

  Future getUnitOfMeasurementID(String unitOfMeasurement) async {
    return await Firestore.instance
        .collection('unitOfMeasurement')
        .where("code", isEqualTo: unitOfMeasurement)
        .limit(1)
        .getDocuments()
        .then(
      (value) {
        if (value.documents.length > 0) {
          return value.documents[0].documentID;
        } else {
          return null;
        }
      },
    );
  }

  Future getUnitOfMeasurement(String unitOfMeasurementID) async {
    DocumentReference docRef = Firestore.instance
        .collection('unitOfMeasurement')
        .document(unitOfMeasurementID);
    DocumentSnapshot doc = await docRef.get();
    String unit = doc.data['code'];
    return unit;
  }

  Future addUnit(String unitNameInputValue, String unitCodeInputValue) async {
    if (await getUnitOfMeasurementID(unitCodeInputValue) == null) {
      return await Firestore.instance
          .collection('unitOfMeasurement')
          .add({'name': unitNameInputValue, 'code': unitCodeInputValue});
    } else {
      return null;
    }
  }

  Future doesUnitExist(String unitCodeInputValue) async {
    return await Firestore.instance
        .collection('unitOfMeasurement')
        .where("code", isEqualTo: unitCodeInputValue)
        .limit(1)
        .getDocuments()
        .then(
      (value) {
        if (value.documents.length > 0) {
          return true;
        } else {
          return false;
        }
      },
    );
  }

  Future getRetailerID(String retailer) async {
    return await Firestore.instance
        .collection('retailer')
        .where("name", isEqualTo: retailer)
        .limit(1)
        .getDocuments()
        .then(
      (value) {
        if (value.documents.length > 0) {
          return value.documents[0].documentID;
        } else {
          return null;
        }
      },
    );
  }

  Future getRetailer(String retailerID) async {
    DocumentReference docRef =
        Firestore.instance.collection('retailer').document(retailerID);
    DocumentSnapshot doc = await docRef.get();
    String retailer = doc.data['name'];
    return retailer;
  }

  Future getRetailerBranches(String retailer) async {
    return await Firestore.instance
        .collection('retailer')
        .where("name", isEqualTo: retailer)
        .limit(1)
        .getDocuments()
        .then(
      (value) {
        if (value.documents.length > 0) {
          return value.documents[0].data['retailerBranches'];
        } else {
          return null;
        }
      },
    );
  }

  Future doesRetailerExist(String retailer) async {
    return await Firestore.instance
        .collection('retailer')
        .where("name", isEqualTo: retailer)
        .limit(1)
        .getDocuments()
        .then(
      (value) {
        if (value.documents.length > 0) {
          return true;
        } else {
          return false;
        }
      },
    );
  }

  Future doesRetailerBranchExist(String retailer, String branchName) async {
    String retailerID = await getRetailerID(retailer);
    DocumentSnapshot variable = await Firestore.instance
        .collection('retailer')
        .document(retailerID)
        .get();
    List branches = variable.data['retailerBranches'];
    if (branches.contains(branchName)) {
      return true;
    } else {
      return false;
    }
  }

  Future addRetailer(String retailer) async {
    if (await getRetailerID(retailer) == null) {
      return await Firestore.instance
          .collection('retailer')
          .add({'name': retailer, 'retailerBranches': []});
    } else {
      return null;
    }
  }

  Future addRetailerBranch(String retailer, String branch) async {
    List branches = await getBranchesList(retailer);
    String retailerID = await getRetailerID(retailer);
    if (!branches.contains(branch)) {
      return await Firestore.instance
          .collection('retailer')
          .document(retailerID)
          .updateData({
        'retailerBranches': FieldValue.arrayUnion([branch])
      });
    } else {
      return null;
    }
  }

  getBranchesList(String retailer) async {
    String retailerID = await getRetailerID(retailer);
    DocumentReference docRef =
        Firestore.instance.collection('retailer').document(retailerID);
    DocumentSnapshot doc = await docRef.get();
    List branches = doc.data['retailerBranches'];
    return branches;
  }

  // assumes barcodeID is unique
  Future doesProductExist(String barcodeID) async {
    return await Firestore.instance
        .collection('product')
        .where("barcodeID", isEqualTo: barcodeID)
        .limit(1)
        .getDocuments()
        .then(
      (value) {
        if (value.documents.length > 0) {
          return true;
        } else {
          return false;
        }
      },
    );
  }

  Future addProduct(
      String barcodeID,
      String category,
      String name,
      double price,
      double quantity,
      String unitOfMeasurement,
      String retailer,
      String retailerBranch) async {
    return await Firestore.instance.collection('product').add({
      'barcodeID': barcodeID,
      'categoryID': await getCategoryID(category),
      'category': category,
      'name': name,
      'price': price,
      'quantity': quantity,
      'unitOfMeasurementID': await getUnitOfMeasurementID(unitOfMeasurement),
      'unitOfMeasurement': unitOfMeasurement,
      'retailerID': await getRetailerID(retailer),
      'retailer': retailer,
      'retailerBranch': retailerBranch
    });
  }
}

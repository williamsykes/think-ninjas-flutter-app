import 'package:flutter/material.dart';
import 'package:think_ninjas_barcode/screens/authenticate/sign_in.dart';
import 'package:think_ninjas_barcode/screens/authenticate/register.dart';

class Authenticate extends StatefulWidget {
  @override
  _AuthenticateState createState() => _AuthenticateState();
}

class _AuthenticateState extends State<Authenticate> {
  bool userSignedIn = true;
  void toggleView() {
    // reverse value for userSignedIn
    setState(() => userSignedIn = !userSignedIn);
  }

  @override
  Widget build(BuildContext context) {
    if (userSignedIn) {
      return SignIn(toggleView: toggleView);
    } else {
      return Register(toggleView: toggleView);
    }
  }
}

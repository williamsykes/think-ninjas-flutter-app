import 'package:flutter/material.dart';
import 'package:think_ninjas_barcode/screens/authenticate/authenticate.dart';
import 'package:provider/provider.dart';
import 'package:think_ninjas_barcode/models/user.dart';
import 'package:think_ninjas_barcode/screens/home/home_page.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    // return either Home or Authenticate Widget
    if (user == null) {
      return Authenticate();
    } else {
      return HomePage();
    }
  }
}

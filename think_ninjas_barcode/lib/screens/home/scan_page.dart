import 'package:flutter/services.dart';
import 'package:think_ninjas_barcode/models/category.dart';
import 'package:think_ninjas_barcode/models/product.dart';
import 'package:think_ninjas_barcode/models/retailer.dart';
import 'package:think_ninjas_barcode/models/unit_of_measurement.dart';
import 'package:think_ninjas_barcode/models/userScan.dart';
import 'package:think_ninjas_barcode/services/database.dart';
import 'package:barcode_scan/platform_wrapper.dart';
import 'package:flutter/material.dart';
import 'package:think_ninjas_barcode/models/user.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:think_ninjas_barcode/shared/constants.dart';
import 'package:think_ninjas_barcode/shared/decimal_formatter.dart';
import 'package:think_ninjas_barcode/shared/dialogs.dart';

class ScanPage extends StatefulWidget {
  @override
  _ScanPageState createState() => _ScanPageState();
}

class _ScanPageState extends State<ScanPage> {
  var scanResult;
  String barcodeID = "";
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  showInputDialogForProduct(
      BuildContext context,
      String userUID,
      List<Product> products,
      List<String> categoriesList,
      List<String> unitList,
      List<String> retailerList,
      List<String> branchList) {
    String name;
    String category;
    double price;
    double quantity;
    String unitOfMeasurement;
    String retailer;
    String retailerBranch;

    String categoryValue = "Select Category";
    String unitValue = "Select Unit";
    String retailerValue = "Select Retailer";
    String branchValue = "Select Branch";

    String categoryInputValue;
    String unitNameInputValue;
    String unitCodeInputValue;
    String retailerInputValue;
    String branchInputValue;

    final _mainKey = GlobalKey<FormState>();
    final _categoryKey = GlobalKey<FormState>();
    final _retailerKey = GlobalKey<FormState>();
    final _unitKey = GlobalKey<FormState>();

    var _categoryController = TextEditingController();
    var _unitNameController = TextEditingController();
    var _unitCodeController = TextEditingController();
    var _retailerController = TextEditingController();
    var _branchController = TextEditingController();

    bool unitFormEnabled = false;
    bool unitInputEnabled = false;
    bool categoryFormEnabled = true;
    bool updateCategoryWithUnit = false;
    bool retailerFormEnabled = true;
    bool branchFormEnabled = false;

    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return SingleChildScrollView(
            child: AlertDialog(
              content: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
                  return Form(
                    key: _mainKey,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Column(
                          children: <Widget>[
                            Text("Enter Product Details Here:"),
                            SizedBox(
                              height: 30,
                            ),
                            Column(
                              children: <Widget>[
                                Text("Name: "),
                                SizedBox(
                                  child: TextFormField(
                                    onChanged: (value) {
                                      setState(() => name = value);
                                    },
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return 'Please enter product name';
                                      }
                                      return null;
                                    },
                                    decoration: InputDecoration(
                                      contentPadding: EdgeInsets.symmetric(
                                          vertical: 10.0, horizontal: 10.0),
                                      border: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.amber[600],
                                            width: 1.0),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text("Quantity:"),
                            TextFormField(
                              inputFormatters: [
                                DecimalTextInputFormatter(decimalRange: 2)
                              ],
                              keyboardType: TextInputType.numberWithOptions(
                                  decimal: true),
                              onChanged: (val) {
                                setState(() => quantity = double.parse(val));
                              },
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Enter quantity';
                                }
                                return null;
                              },
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 10.0, horizontal: 10.0),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.amber[600], width: 1.0),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Text("Price:"),
                            SizedBox(
                              child: TextFormField(
                                inputFormatters: [
                                  DecimalTextInputFormatter(decimalRange: 2)
                                ],
                                keyboardType: TextInputType.numberWithOptions(
                                    decimal: true),
                                onChanged: (val) {
                                  setState(() => price = double.parse(val));
                                },
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Please enter product price';
                                  }
                                  return null;
                                },
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 10.0),
                                  border: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.amber[600], width: 1.0),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 50,
                            ),
                            Column(
                              children: <Widget>[
                                Text("Category:"),
                                DropdownButtonFormField<String>(
                                  isExpanded: true,
                                  value: categoryValue,
                                  items: categoriesList.map((String value) {
                                    return new DropdownMenuItem<String>(
                                      value: value,
                                      child: new Text(value),
                                    );
                                  }).toList(),
                                  onChanged: (String value) async {
                                    setState(() {
                                      categoryValue = value;

                                      if (categoryValue != "Select Category") {
                                        category = value;
                                        unitFormEnabled = false;
                                        unitInputEnabled = false;
                                        categoryFormEnabled = false;
                                      } else {
                                        unitFormEnabled = true;
                                        unitInputEnabled = true;
                                        categoryFormEnabled = true;
                                      }
                                    });

                                    if (categoryValue != "Select Category" &&
                                        !await DatabaseService()
                                            .doesCategoryHaveUnit(
                                                categoryValue)) {
                                      setState(() {
                                        updateCategoryWithUnit = true;
                                        unitFormEnabled = true;
                                        unitInputEnabled = true;
                                      });
                                    } else {
                                      setState(() {
                                        updateCategoryWithUnit = false;
                                        unitFormEnabled = false;
                                        unitInputEnabled = false;
                                      });
                                    }
                                  },
                                  validator: (value) =>
                                      value == "Select Category"
                                          ? 'Please select category'
                                          : null,
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Form(
                                  key: _categoryKey,
                                  child: Container(
                                    padding: EdgeInsets.all(20),
                                    decoration: addProductContainers,
                                    child: Column(
                                      children: [
                                        Text("Add New Category: "),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Text("Category Name: "),
                                        SizedBox(
                                          child: TextFormField(
                                            enabled: categoryFormEnabled,
                                            controller: _categoryController,
                                            onChanged: (value) {
                                              setState(() {
                                                categoryInputValue = value;
                                                category = value;
                                              });
                                            },
                                            validator: (value) {
                                              if (value.isEmpty) {
                                                return 'Please enter product category';
                                              }
                                              return null;
                                            },
                                            decoration: InputDecoration(
                                              contentPadding:
                                                  EdgeInsets.symmetric(
                                                      vertical: 10.0,
                                                      horizontal: 10.0),
                                              border: OutlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Colors.amber[600],
                                                    width: 1.0),
                                              ),
                                              suffixIcon: IconButton(
                                                onPressed: () async {
                                                  if (_categoryKey.currentState
                                                      .validate()) {
                                                    if (!await DatabaseService()
                                                        .doesCategoryExist(
                                                            categoryInputValue)) {
                                                      await DatabaseService()
                                                          .addCategory(
                                                              categoryInputValue);
                                                      if (!await DatabaseService()
                                                          .doesCategoryHaveUnit(
                                                              categoryInputValue)) {
                                                        setState(() {
                                                          unitFormEnabled =
                                                              true;
                                                          unitInputEnabled =
                                                              true;
                                                        });
                                                      }
                                                      setState(() {
                                                        categoriesList.add(
                                                            categoryInputValue);
                                                      });
                                                    }
                                                    setState(() {
                                                      categoryValue =
                                                          categoryInputValue;
                                                      category =
                                                          categoryInputValue;
                                                      categoryFormEnabled =
                                                          false;
                                                    });
                                                    _categoryController.clear();
                                                  }
                                                },
                                                icon: Icon(Icons.add_circle),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 50,
                            ),
                            Column(
                              children: <Widget>[
                                Text("Unit:"),
                                DropdownButtonFormField<String>(
                                  isExpanded: true,
                                  value: unitValue,
                                  items: unitList.map((String value) {
                                    return new DropdownMenuItem<String>(
                                      value: value,
                                      child: new Text(value),
                                    );
                                  }).toList(),
                                  onChanged: unitInputEnabled
                                      ? (String value) async {
                                          setState(() {
                                            unitValue = value;
                                          });

                                          if (unitValue != "Select Unit") {
                                            if (!await DatabaseService()
                                                .doesCategoryHaveUnit(
                                                    categoryInputValue)) {
                                              await DatabaseService()
                                                  .updateCategoryWithUnit(
                                                      categoryInputValue,
                                                      value);
                                            }

                                            setState(() {
                                              unitOfMeasurement = value;
                                              unitInputEnabled = false;
                                              unitFormEnabled = false;
                                            });
                                          } else {
                                            setState(() {
                                              unitFormEnabled = true;
                                              unitFormEnabled = true;
                                            });
                                          }
                                        }
                                      : null,
                                  validator: unitFormEnabled
                                      ? ((value) => value == "Select Unit"
                                          ? 'Please select unit'
                                          : null)
                                      : null,
                                ),
                                SizedBox(
                                  height: 30,
                                ),
                                Form(
                                  key: _unitKey,
                                  child: Container(
                                    padding: EdgeInsets.all(20),
                                    decoration: addProductContainers,
                                    child: Column(
                                      children: <Widget>[
                                        Text("Add New Unit: "),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Text("Unit Name: "),
                                        SizedBox(
                                          child: TextFormField(
                                            enabled: unitFormEnabled,
                                            controller: _unitNameController,
                                            onChanged: (value) {
                                              setState(() {
                                                unitNameInputValue = value;
                                              });
                                            },
                                            decoration: InputDecoration(
                                              contentPadding:
                                                  EdgeInsets.symmetric(
                                                      vertical: 10.0,
                                                      horizontal: 10.0),
                                              border: OutlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Colors.amber[600],
                                                    width: 1.0),
                                              ),
                                            ),
                                            validator: (value) {
                                              if (value.isEmpty) {
                                                return 'Please enter name';
                                              }
                                              return null;
                                            },
                                          ),
                                        ),
                                        Text("Unit Code: "),
                                        SizedBox(
                                          child: TextFormField(
                                            enabled: unitFormEnabled,
                                            controller: _unitCodeController,
                                            onChanged: (value) {
                                              setState(() {
                                                unitOfMeasurement = value;
                                                unitCodeInputValue = value;
                                              });
                                            },
                                            decoration: InputDecoration(
                                              contentPadding:
                                                  EdgeInsets.symmetric(
                                                      vertical: 10.0,
                                                      horizontal: 10.0),
                                              border: OutlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Colors.amber[600],
                                                    width: 1.0),
                                              ),
                                            ),
                                            validator: (value) {
                                              if (value.isEmpty) {
                                                return 'Please enter code';
                                              }
                                              return null;
                                            },
                                          ),
                                        ),
                                        new FlatButton(
                                          onPressed: unitFormEnabled
                                              ? (() async {
                                                  if (_unitKey.currentState
                                                      .validate()) {
                                                    if (!await DatabaseService()
                                                        .doesUnitExist(
                                                            unitCodeInputValue)) {
                                                      await DatabaseService()
                                                          .addUnit(
                                                              unitNameInputValue,
                                                              unitCodeInputValue);
                                                      setState(() {
                                                        unitList.add(
                                                            unitCodeInputValue);
                                                      });
                                                    }

                                                    setState(() {
                                                      unitValue =
                                                          unitCodeInputValue;
                                                      unitOfMeasurement =
                                                          unitCodeInputValue;
                                                      unitFormEnabled = false;
                                                    });

                                                    if (updateCategoryWithUnit) {
                                                      await DatabaseService()
                                                          .updateCategoryWithUnit(
                                                              categoryInputValue,
                                                              unitCodeInputValue);
                                                    }
                                                  }
                                                  _unitNameController.clear();
                                                  _unitCodeController.clear();
                                                })
                                              : null,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: <Widget>[
                                              Text("Add"),
                                              Icon(Icons.add_circle)
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 50,
                            ),
                            Column(
                              children: <Widget>[
                                Text("Retailer:"),
                                DropdownButtonFormField<String>(
                                  isExpanded: true,
                                  value: retailerValue,
                                  items: retailerList.map((String value) {
                                    return new DropdownMenuItem<String>(
                                      value: value,
                                      child: new Text(
                                        value,
                                      ),
                                    );
                                  }).toList(),
                                  onChanged: (String value) async {
                                    setState(() {
                                      retailerValue = value;
                                      branchValue = "Select Branch";
                                      branchList = ['Select Branch'];
                                    });

                                    List retailerBranches = [];

                                    if (retailerValue != "Select Retailer") {
                                      retailerBranches = await DatabaseService()
                                          .getRetailerBranches(retailerValue);
                                    }

                                    setState(() {
                                      if (retailerValue == "Select Retailer") {
                                        retailerFormEnabled = true;
                                        branchFormEnabled = false;
                                      } else {
                                        retailerValue = value;
                                        retailer = value;
                                        retailerFormEnabled = false;
                                        branchFormEnabled = true;
                                        for (var branches in retailerBranches) {
                                          branchList.add(branches);
                                        }
                                      }
                                    });
                                  },
                                  validator: (value) =>
                                      value == "Select Retailer"
                                          ? 'Please select retailer'
                                          : null,
                                ),
                                SizedBox(
                                  width: 30,
                                ),
                                Text("Branch:"),
                                DropdownButtonFormField<String>(
                                  isExpanded: true,
                                  value: branchValue,
                                  items: branchList.map((String value) {
                                    return new DropdownMenuItem<String>(
                                      value: value,
                                      child: new Text(value),
                                    );
                                  }).toList(),
                                  onChanged: (String value) {
                                    setState(() {
                                      branchValue = value;

                                      if (branchValue != "Select Branch") {
                                        retailerBranch = value;
                                        branchFormEnabled = false;
                                      } else {
                                        branchFormEnabled = true;
                                      }
                                    });
                                  },
                                  validator: (value) => value == "Select Branch"
                                      ? 'Please select branch'
                                      : null,
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Form(
                                  key: _retailerKey,
                                  child: Container(
                                    padding: EdgeInsets.all(20),
                                    decoration: addProductContainers,
                                    child: Column(
                                      children: [
                                        Text("Add New Retailer: "),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Text("Retailer Name: "),
                                        SizedBox(
                                          child: TextFormField(
                                            enabled: retailerFormEnabled,
                                            controller: _retailerController,
                                            onChanged: (value) {
                                              setState(() {
                                                retailer = value;
                                                //retailerValue = value;
                                                retailerInputValue = value;
                                              });
                                            },
                                            decoration: InputDecoration(
                                              contentPadding:
                                                  EdgeInsets.symmetric(
                                                      vertical: 10.0,
                                                      horizontal: 10.0),
                                              border: OutlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Colors.amber[600],
                                                    width: 1.0),
                                              ),
                                              suffixIcon: IconButton(
                                                onPressed: () async {
                                                  if (await DatabaseService()
                                                          .addRetailer(
                                                              retailerInputValue) !=
                                                      null) {
                                                    setState(() {
                                                      retailerList.add(
                                                          retailerInputValue);
                                                      retailer =
                                                          retailerInputValue;
                                                      retailerValue = retailer;
                                                      retailerFormEnabled =
                                                          false;
                                                    });
                                                  }
                                                  _retailerController.clear();
                                                },
                                                icon: Icon(Icons.add_circle),
                                              ),
                                            ),
                                            validator: (value) {
                                              if (value.isEmpty) {
                                                return 'Please enter retailer';
                                              }
                                              return null;
                                            },
                                          ),
                                        ),
                                        Text("Branch Name: "),
                                        SizedBox(
                                          child: TextFormField(
                                            enabled: branchFormEnabled,
                                            controller: _branchController,
                                            onChanged: (value) {
                                              setState(() {
                                                retailerBranch = value;
                                                branchInputValue = value;
                                              });
                                            },
                                            decoration: InputDecoration(
                                              contentPadding:
                                                  EdgeInsets.symmetric(
                                                      vertical: 10.0,
                                                      horizontal: 10.0),
                                              border: OutlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Colors.amber[600],
                                                    width: 1.0),
                                              ),
                                              suffixIcon: IconButton(
                                                onPressed: () async {
                                                  if (!await DatabaseService()
                                                      .doesRetailerBranchExist(
                                                          retailer,
                                                          branchInputValue)) {
                                                    await DatabaseService()
                                                        .addRetailerBranch(
                                                            retailer,
                                                            branchInputValue);

                                                    setState(() {
                                                      branchList.add(
                                                          branchInputValue);
                                                    });
                                                  }
                                                  setState(() {
                                                    retailerBranch =
                                                        branchInputValue;
                                                    branchValue =
                                                        branchInputValue;
                                                    branchFormEnabled = false;
                                                  });
                                                  _branchController.clear();
                                                },
                                                icon: Icon(Icons.add_circle),
                                              ),
                                            ),
                                            validator: (value) {
                                              if (value.isEmpty) {
                                                return 'Please enter branch';
                                              }
                                              if (retailerValue ==
                                                      "Select Retailer" &&
                                                  branchValue ==
                                                      "Select Branch") {
                                                return "Please add some input";
                                              }
                                              return null;
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 50,
                            ),
                            new Center(
                              child: new Row(
                                children: <Widget>[
                                  new SizedBox(
                                    width: 20,
                                  ),
                                  new FlatButton(
                                    onPressed: () async {
                                      if (_mainKey.currentState.validate()) {
                                        Dialogs.showLoadingDialog(
                                            context, _keyLoader);

                                        if (updateCategoryWithUnit) {
                                          await DatabaseService()
                                              .updateCategoryWithUnit(
                                                  categoryInputValue,
                                                  unitCodeInputValue);
                                        }

                                        await DatabaseService().addProduct(
                                            barcodeID,
                                            category,
                                            name,
                                            price,
                                            quantity,
                                            unitOfMeasurement,
                                            retailer,
                                            retailerBranch);
                                        await DatabaseService()
                                            .addUserScans(userUID, barcodeID);
                                        setState(() {
                                          barcodeID = "";
                                        });
                                        Navigator.of(context).pop();
                                        Navigator.of(context).pop();
                                      } else {}
                                    },
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Text("Add"),
                                        Icon(Icons.add_circle)
                                      ],
                                    ),
                                  ),
                                  new FlatButton(
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text("Cancel"),
                                        Icon(Icons.cancel)
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
          );
        });
  }

  display(
      String userUID,
      List<Product> products,
      List scans,
      List<Product> userScannedProducts,
      List<String> categoriesList,
      List<String> unitList,
      List<String> retailerList,
      List<String> branchList) {
    List<Widget> scanButton = [
      RaisedButton(
        color: Colors.amber[800].withOpacity(1),
        shape: RoundedRectangleBorder(
            side: BorderSide(
                color: Colors.grey[800], width: 1, style: BorderStyle.solid),
            borderRadius: BorderRadius.circular(20)),
        child: Text(
          "Scan Barcode",
          style: TextStyle(color: Colors.grey[800]),
        ),
        onPressed: () async {
          scanResult = await BarcodeScanner.scan();
          if (scanResult != null && scanResult.type.toString() != 'Cancelled') {
            setState(() {
              barcodeID = scanResult.rawContent;
            });
            Dialogs.showLoadingDialog(context, _keyLoader);
            if (await DatabaseService().doesProductExist(barcodeID)) {
              await DatabaseService().addUserScans(userUID, barcodeID);
              Navigator.of(context).pop();
              setState(() {
                barcodeID = "";
              });
            } else {
              Navigator.of(context).pop();
              showInputDialogForProduct(context, userUID, products,
                  categoriesList, unitList, retailerList, branchList);
            }
          }
        },
      ),
    ];

    if (scans.length == 0) {
      List<Widget> noScanStructure = [
        Text((() {
          return "No barcode scan data available";
        })()),
        SizedBox(
          height: 20,
        ),
      ];

      return noScanStructure + scanButton;
    } else {
      Product latestScannedProduct = userScannedProducts.last;
      double pricePerUnit =
          latestScannedProduct.price / latestScannedProduct.quantity;
      double averageCategoryPrice = 0;
      int count = 0;

      // get all user scanned products:
      for (var product in products) {
        if (product.category == latestScannedProduct.category) {
          averageCategoryPrice += (product.price / product.quantity);
          count++;
        }
      }

      averageCategoryPrice = averageCategoryPrice / count;
      bool isPriceBetter = pricePerUnit > averageCategoryPrice;

      List<Widget> scanStructure = [
        Container(
          padding: EdgeInsets.all(20),
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.grey[800],
              width: 3,
            ),
            borderRadius: BorderRadius.all(Radius.circular(20)),
            color: Colors.white70.withOpacity(1),
          ),
          child: Column(
            children: <Widget>[
              Text(
                "Latest Scan:",
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Text((() {
                return latestScannedProduct.name;
              })()),
              Text(
                latestScannedProduct.barcodeID,
              ),
              Text(
                latestScannedProduct.category,
              ),
              Text(
                latestScannedProduct.quantity.toStringAsFixed(2) +
                    " " +
                    latestScannedProduct.unitOfMeasurement,
              ),
              Text(
                latestScannedProduct.retailer +
                    " - " +
                    latestScannedProduct.retailerBranch,
              ),
              SizedBox(
                height: 30,
              ),
              Text(
                "Price per " +
                    latestScannedProduct.unitOfMeasurement +
                    ": R" +
                    pricePerUnit.toStringAsFixed(2) +
                    (isPriceBetter
                        ? String.fromCharCode(0x2191)
                        : (pricePerUnit == averageCategoryPrice)
                            ? ""
                            : String.fromCharCode(0x2193)),
                style: TextStyle(
                    color: isPriceBetter
                        ? Colors.red
                        : (pricePerUnit == averageCategoryPrice)
                            ? Colors.grey[800]
                            : Colors.green),
              ),
              Text(
                " Avg. Price per " +
                    latestScannedProduct.unitOfMeasurement +
                    ": R" +
                    averageCategoryPrice.toStringAsFixed(2) +
                    (isPriceBetter
                        ? String.fromCharCode(0x2193)
                        : (pricePerUnit == averageCategoryPrice)
                            ? ""
                            : String.fromCharCode(0x2191)),
                style: TextStyle(
                    color: isPriceBetter
                        ? Colors.green
                        : (pricePerUnit == averageCategoryPrice)
                            ? Colors.grey[800]
                            : Colors.red),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 20,
        ),
      ];

      return scanStructure + scanButton;
    }
  }

  @override
  Widget build(BuildContext context) {
    //final users = Provider.of<QuerySnapshot>(context);
    final user = Provider.of<User>(context);

    final products = Provider.of<List<Product>>(context) ?? [];
    final userScans = Provider.of<List<UserScan>>(context) ?? [];
    final categories = Provider.of<List<Category>>(context) ?? [];
    final units = Provider.of<List<Unit>>(context) ?? [];
    final retailers = Provider.of<List<Retailer>>(context) ?? [];

    List<String> categoriesList = <String>['Select Category'];
    for (var doc in categories) {
      categoriesList.add(doc.name);
    }

    List<String> unitList = <String>['Select Unit'];
    for (var doc in units) {
      unitList.add(doc.code);
    }

    List<String> retailerList = <String>['Select Retailer'];
    List<String> branchList = <String>['Select Branch'];
    for (var doc in retailers) {
      retailerList.add(doc.name);
    }

    List<String> scans = new List<String>();

    for (var doc in userScans) {
      if (doc.userUID == user.userUID) {
        for (String s in doc.scans) {
          scans.add(s);
        }
      }
    }

    List<String> productCodes = new List<String>();
    for (var doc in products) {
      productCodes.add(doc.barcodeID);
    }

    List<Product> userScannedProducts = new List<Product>();

    for (var scan in scans) {
      userScannedProducts
          .add(products.firstWhere((product) => product.barcodeID == scan));
    }

    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: display(user.userUID, products, scans, userScannedProducts,
            categoriesList, unitList, retailerList, branchList),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:think_ninjas_barcode/services/auth.dart';
import 'package:think_ninjas_barcode/screens/home/scan_page.dart';

class Home extends StatelessWidget {
  final AuthService _auth = AuthService();
  var scanResult;
  String result = "no result";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[850],
      appBar: AppBar(
        backgroundColor: Colors.amber[900],
        elevation: 0.0,
        title: Text("Barcode App"),
        actions: <Widget>[
          FlatButton.icon(
            icon: Icon(Icons.person),
            label: Text('Logout'),
            onPressed: () async {
              await _auth.signOut();
            },
          ),
        ],
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/bg.jpg'),
            fit: BoxFit.cover,
          ),
        ),
        child: ScanPage(),
      ),
    );
  }
}

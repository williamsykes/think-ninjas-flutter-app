import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:think_ninjas_barcode/models/category.dart';
import 'package:think_ninjas_barcode/models/product.dart';
import 'package:think_ninjas_barcode/models/retailer.dart';
import 'package:think_ninjas_barcode/models/unit_of_measurement.dart';
import 'package:think_ninjas_barcode/models/userScan.dart';
import 'package:think_ninjas_barcode/screens/home/scan_history.dart';
import 'package:think_ninjas_barcode/services/auth.dart';
import 'package:think_ninjas_barcode/screens/home/scan_page.dart';
import 'package:think_ninjas_barcode/services/database.dart';
import 'package:think_ninjas_barcode/shared/loading.dart';

bool loading = false;

class HomePage extends StatefulWidget {
  setLoading(bool val) {
    loading = val;
  }

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  final AuthService _auth = AuthService();
  var scanResult;
  String result = "no result";

  TabController tabController;

  @override
  void initState() {
    super.initState();
    tabController = new TabController(vsync: this, length: 2);
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MultiProvider(
      providers: [
        StreamProvider<List<Product>>.value(
          value: DatabaseService().products,
        ),
        StreamProvider<List<UserScan>>.value(
          value: DatabaseService().userScans,
        ),
        StreamProvider<List<Category>>.value(
          value: DatabaseService().categories,
        ),
        StreamProvider<List<Unit>>.value(
          value: DatabaseService().units,
        ),
        StreamProvider<List<Retailer>>.value(
          value: DatabaseService().retailers,
        ),
      ],
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        backgroundColor: Colors.grey[850],
        appBar: AppBar(
            backgroundColor: Colors.amber[900],
            elevation: 0.0,
            title: Text("Barcode App"),
            actions: <Widget>[
              FlatButton.icon(
                icon: Icon(Icons.person),
                label: Text('Logout'),
                onPressed: () async {
                  await _auth.signOut();
                },
              ),
            ],
            bottom: new TabBar(
              controller: tabController,
              labelColor: Colors.grey[800],
              indicatorColor: Colors.grey[800],
              tabs: [
                Tab(
                  text: "Current Scan",
                  icon: new Icon(Icons.home),
                ),
                Tab(
                  text: "Scan History",
                  icon: new Icon(Icons.history),
                ),
              ],
            )),
        body: new Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/bg.jpg'),
              fit: BoxFit.cover,
            ),
          ),
          child: new TabBarView(
            controller: tabController,
            children: <Widget>[
              loading ? new Loading() : new ScanPage(),
              new ScanHistory(),
            ],
          ),
        ),
      ),
    );
  }
}

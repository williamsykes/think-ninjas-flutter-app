import 'package:think_ninjas_barcode/models/userScan.dart';
import 'package:think_ninjas_barcode/services/database.dart';
import 'package:flutter/material.dart';
import 'package:think_ninjas_barcode/models/user.dart';
import 'package:think_ninjas_barcode/models/product.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:think_ninjas_barcode/shared/dialogs.dart';

class ScanHistory extends StatefulWidget {
  @override
  _ScanHistoryState createState() => _ScanHistoryState();
}

class _ScanHistoryState extends State<ScanHistory> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);
    final userScans = Provider.of<List<UserScan>>(context) ?? [];
    final products = Provider.of<List<Product>>(context) ?? [];
    final GlobalKey<State> _keyLoader = new GlobalKey<State>();

    List<String> scans = new List<String>();
    for (var doc in userScans) {
      if (doc.userUID == user.userUID) {
        for (String s in doc.scans) {
          scans.add(s);
        }
      }
    }

    List<String> productCodes = new List<String>();
    for (var doc in products) {
      productCodes.add(doc.barcodeID);
    }

    List<Product> userScannedProducts = new List<Product>();

    for (var scan in scans) {
      userScannedProducts
          .add(products.firstWhere((product) => product.barcodeID == scan));
    }

    return ListView.builder(
        itemCount: scans.length,
        itemBuilder: (context, index) {
          return ListTile(
            title: new Card(
              elevation: 5,
              shape: RoundedRectangleBorder(
                  side: BorderSide(
                      color: Colors.grey[800],
                      width: 1,
                      style: BorderStyle.solid),
                  borderRadius: BorderRadius.circular(20)),
              child: new Row(
                children: <Widget>[
                  new Container(
                    margin: new EdgeInsets.only(left: 10),
                    child: new Text(
                      (index + 1).toString(),
                    ),
                  ),
                  new SizedBox(
                    width: 20,
                  ),
                  new Container(
                    alignment: Alignment.centerLeft,
                    margin: new EdgeInsets.only(top: 10, bottom: 10),
                    child: Align(
                      alignment: Alignment.center,
                      child: Container(
                        child: Text(
                          userScannedProducts.elementAt(index).name,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            onTap: () async {
              Dialogs.showLoadingDialog(context, _keyLoader);
              await showDialog(
                  barrierDismissible: false,
                  context: context,
                  child: new CupertinoAlertDialog(
                    title: new Column(
                      children: <Widget>[
                        new Text(userScannedProducts.elementAt(index).name),
                      ],
                    ),
                    content: Column(
                      children: <Widget>[
                        new Text(
                          "Barcode: " +
                              userScannedProducts.elementAt(index).barcodeID,
                        ),
                        new Text(
                          "Category: " +
                              await DatabaseService().getCategory(
                                  userScannedProducts
                                      .elementAt(index)
                                      .categoryID),
                        ),
                        new Text(
                          "Quantity: " +
                              userScannedProducts
                                  .elementAt(index)
                                  .quantity
                                  .toString() +
                              " " +
                              await DatabaseService().getUnitOfMeasurement(
                                  userScannedProducts
                                      .elementAt(index)
                                      .unitOfMeasurementID),
                        ),
                        new Text(
                          "Price: " +
                              "R" +
                              userScannedProducts
                                  .elementAt(index)
                                  .price
                                  .toString(),
                        ),
                        new Text(
                          "Retailer: " +
                              await DatabaseService().getRetailer(
                                  userScannedProducts
                                      .elementAt(index)
                                      .retailerID),
                        ),
                        new Text(
                          "Branch: " +
                              userScannedProducts
                                  .elementAt(index)
                                  .retailerBranch,
                        ),
                      ],
                    ),
                    actions: <Widget>[
                      new FlatButton(
                        onPressed: () {
                          /* pop is called twice here since whenever we click 
                           * on an area (even a button) on the card, it 
                           * initiates the loading screen. So we need to pop 
                           * once to get off the card context, and another pop 
                           * to remove the loading screen
                           */
                          Navigator.of(context).pop();
                          Navigator.of(context).pop();
                        },
                        child: new Text(
                          "OK",
                        ),
                      ),
                      new FlatButton(
                        onPressed: () async {
                          Dialogs.showLoadingDialog(context, _keyLoader);
                          await DatabaseService().removeUserScans(user.userUID,
                              userScannedProducts.elementAt(index).barcodeID);
                          //triple threat
                          //which is worse: having three pops or non-loading screens
                          //see no evil, speak no evil
                          Navigator.of(context).pop();
                          Navigator.of(context).pop();
                          Navigator.of(context).pop();
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text("Delete"),
                            Icon(Icons.cancel)
                          ],
                        ),
                      ),
                    ],
                  ));
            },
          );
        });
  }
}

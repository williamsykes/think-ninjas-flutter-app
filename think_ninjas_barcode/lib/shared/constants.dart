import 'package:flutter/material.dart';

const textInputDecoration = InputDecoration(
  fillColor: Colors.white,
  filled: true,
  enabledBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Colors.white, width: 2),
  ),
  focusedBorder:
      OutlineInputBorder(borderSide: BorderSide(color: Colors.amber, width: 2)),
);

BoxDecoration addProductContainers = BoxDecoration(
  border: Border.all(
    color: Colors.grey[800],
    width: 1,
  ),
  borderRadius: BorderRadius.all(Radius.circular(5)),
  color: Colors.white70.withOpacity(1),
);

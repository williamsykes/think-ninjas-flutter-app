class User {
  final String userUID;

  User({this.userUID});
}

class UserData {
  final String userUID;
  final String barcodeID;

  UserData({this.userUID, this.barcodeID});
}

class Product {
  final String barcodeID;
  final String categoryID;
  final String category;
  final String name;
  final double price;
  final double quantity;
  final String unitOfMeasurementID;
  final String unitOfMeasurement;
  final String retailerID;
  final String retailer;
  final String retailerBranch;

  Product({
    this.barcodeID,
    this.categoryID,
    this.category,
    this.name,
    this.price,
    this.quantity,
    this.unitOfMeasurementID,
    this.unitOfMeasurement,
    this.retailerID,
    this.retailer,
    this.retailerBranch,
  });
}

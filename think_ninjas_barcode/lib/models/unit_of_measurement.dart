class Unit {
  final String code;
  final String name;

  Unit({this.code, this.name});
}

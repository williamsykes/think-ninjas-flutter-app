class Category {
  final String name;
  final String unitOfMeasurementID;

  Category({this.name, this.unitOfMeasurementID});
}
